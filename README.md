# Upload Dispatcher

This is Node.js based microservice that monitors a NFS directory and sends upload requests to workers over the NATS message bus.

The operation of his service is as follows:

* The [chokidar](https://github.com/paulmillr/chokidar) package monitors the upload directory and emits events when files change.
* The "add" event causes the dispatcher to publish a message over NATS to the `workers.queue` topic to tell a worker to upload.
* A worker sends a message to the `workers.status` topic when it has started the upload (`uploadStart`), finished the upload (`uploadDone`), or if an error occured with the upload (`uploadError`).
* The dispatcher will respond to the `uploadDone` and `uploadError` messages by deleting the original file.
* The dispatcher writes a log message when it receives a status update message, but in the future these actions could also be recorded in a database.

Currently the `uploadProgress` event is not yet supported by the worker.

<img class="statcounter" src="https://c.statcounter.com/11994331/0/eed1a29e/1/">