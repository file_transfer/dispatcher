const NATS = require('nats'),
      EventEmitter = require('events');

const statusSubTopic = 'workers.status',
      queuePubTopic = 'workers.queue';

class MessageBus extends EventEmitter {
  constructor(msgConfig) {
    super();
    this.servers = this.createServerString(msgConfig);
    this.nats = {};
    this.statusSub = {};
  }
  createServerString(opts){
    var server = (opts.tls) ? 'tls' : 'nats';
    server += '://';

    if (opts.user) {
      server += opts.user || '';
      server += (opts.pass) ? `:${opts.pass}@` : '';
    }
    server += `${opts.host}:${opts.port}`;
    return [server];
  }
  connect(){
    let self = this,
        firstConnect = true;
    this.nats = NATS.connect({servers: this.servers});
    return new Promise((resolve, reject) => {
      self.nats.on('error', (err) => {
        if (firstConnect)
          reject(err); // For inital connect errors
        else
          self.emit('error', err); // for future errors
      });

      self.nats.once('connect', () => {
        firstConnect = false;
        self.nats.subscribe(statusSubTopic, (msg)=>{
          self.emit('status', msg);
        });
        resolve();
      });
    });
  }
  close(){
    this.nats.close();
  }
  dispatchfileUpload(file){
    let message = {
      action: 'fileUpload',
      file: file,
    };
    this.nats.publish(queuePubTopic, JSON.stringify(message));
  }
}

module.exports = MessageBus;