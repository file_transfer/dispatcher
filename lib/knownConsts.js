module.exports = {
  logLevels: {
    fatal: 0,
    error: 1,
    warn: 3,
    info: 4,
    debug: 5,
    trace: 6,
  },
  defaultLogLevel: 'info',
};