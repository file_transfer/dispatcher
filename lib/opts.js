
module.exports = {
  uploadDir: process.env.UPLOAD_DIR || '/tmp',
  logfile: process.env.LOG_FILE || '/tmp/dispatcher.log',
  logLevel: process.env.LOG_LEVEL || 'info',
  logconsole: Boolean(process.env.LOG_CONSOLE),
  msgConfig: {
    tls: (process.env.MSG_TLS) ? true : false,
    user: process.env.MSG_USER,
    pass: process.env.MSG_PASS,
    host: process.env.MSG_HOST || 'localhost',
    port: process.env.MSG_PORT || '4222',
  },
};
