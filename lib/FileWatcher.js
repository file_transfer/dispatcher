const { FSWatcher } = require('chokidar'),
      EventEmitter = require('events'),
      fs = require('fs-extra'),
      {resolve} = require('path');

/**
 * @param {String} folder Directory to watch for changes
 * @param {Object} logger winston logger object
 */
class FileWatcher extends EventEmitter {
  constructor(folder, logger) {
    super();
    this.folder = resolve(folder);
    this.log = logger;
    this.watcher = {};
  }
  start(){
    let self = this,
        opts = {
          awaitWriteFinish: true,
          ignoreInitial: true,
        };
    return new Promise(async (resolve, reject) =>{
      // chokidar does not throw any errors for watching non-existent folder
      // https://github.com/paulmillr/chokidar/issues/462
      try {
        let stat = await fs.stat(self.folder);
        if (! stat.isDirectory())
          throw TypeError(`Path ${this.folder} is not a directory.`);
      } catch (err) {
        return reject(err);
      }
      self.watcher = new FSWatcher(opts)
        .on('ready', ()=>{
          resolve();
        })
        .on('error', (err)=>{
          self.log.error(`Watcher error: ${err.message}`);
          self.log.debug(err);
        })
        .on('add', (name) => {
          self.log.debug(`Watcher event type: Add, name: ${name}`); 
          this.watcher.unwatch(resolve(this.folder, name)); // Prevent ENOSPC limit watchers 
          self.emit('add', name);
        })
        .on('change', (name) => {
          self.log.debug(`Watcher event type: Change, name: ${name}`);
          self.emit('change', name);
        })
        .on('unlink', (name) => {
          self.log.debug(`Watcher event type: Delete, name: ${name}`);
          self.emit('delete', name);
        });
      self.watcher.add(self.folder);
    });
  }

  stop(){
    this.watcher.close();
  }
}

module.exports = FileWatcher;