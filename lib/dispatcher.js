const FileWatcher = require('./FileWatcher'),
      MessageBus = require('./MessageBus'),
      {createLogger, transports, format} = require('winston'),
      kc = require('./knownConsts'),
      path = require('path'),
      hostForm =  require('./formatter');

/**
 * Returns a new instance of dispatcher.
 * @param {Object} opts Object from opts.js file
 *
 */
class Dispatcher {
  constructor(opts) {
    let transport;
    if (opts.logfile && (! opts.logconsole)) {
      transport = new transports.File({filename: opts.logfile});
    } else {
      let consoleOff = new transports.Console();
      if (! opts.logconsole) consoleOff.silent = true;
      transport = consoleOff;
    }
   
    this.log = createLogger({
      format: format.combine(
        hostForm(),
        format.timestamp(), 
        format.json(),
      ),
      level: opts.logLevel || kc.defaultLogLevel,
      levels: kc.logLevels,
      transports: transport,
    });
    this.uploadDir = opts.uploadDir;
    this.watcher = new FileWatcher(this.uploadDir, this.log);
    this.msg = new MessageBus(opts.msgConfig);
    this._statusEventCB = ()=>{};
  }
  set statusEventCB(cb){
    this._statusEventCB = cb;
  }

  // Connects to the message bus and starts monitoring the upload dir
  async start(){
    this.log.info('Starting dispatcher');
    try {
      this.log.debug('Connecting to message bus');
      await this.msg.connect();
    } catch (err) {
      this.log.fatal(`Error connecting to message bus: ${err.message}`);
      this.log.debug(err);
      return Promise.reject(err);
    }
    this.log.debug('Message bus connected');

    try {
      this.log.debug(`Starting monitor of directory: '${this.uploadDir}'`);
      await this.watcher.start();
    } catch (err) {
      this.log.fatal(`Error watching directory: ${err.message}`);
      this.log.debug(err);
      return Promise.reject(err);
    }
    let self = this;
    // New file action
    this.watcher
      .on('add', (name)=>{
        let fileRel = path.relative(this.uploadDir, name);
        self.log.info(`New file '${fileRel}'.`);
        self.msg.dispatchfileUpload(fileRel);
      });
    // Status update action
    this.msg.on('status', this._statusUpdateCB.bind(this));
    return Promise.resolve();
  }
  /* 
   *
   */
  async _statusUpdateCB(message){
    let msg = {};
    try {
      msg = JSON.parse(message);
      // Validate keys are in object
      for (let key of ['action', 'file', 'worker']) {
        if (! msg[key]) throw Error(`Attribute '${key}' not present in message.`);
      }
    } catch (err) {
      this.log.error(`Error parsing JSON from message bus: ${err}`);
      return;
    }

    switch (msg.action) {
      case 'uploadStart':
        this.log.info(`File upload started, file: '${msg.file}', worker: '${msg.worker}'.`);
        this._statusEventCB(msg.action, msg.file, msg.worker);
        break;
      case 'uploadProgress':
        this.log.info(`File '${msg.file}' upload at ${msg.meta}%, worker: '${msg.worker}'.`);
        this._statusEventCB(msg.action, msg.file, msg.worker, msg.meta);
        break;
      case 'uploadDone':
        this.log.info(`File upload completed: '${msg.file}', worker: '${msg.worker}'.`);
        this._statusEventCB(msg.action, msg.file, msg.worker);
        break;
      case 'uploadError':
        this.log.error(
          `File upload error: '${msg.file}', error: '${msg.meta}', worker: '${msg.worker}'.`);
        this._statusEventCB(msg.action, msg.file, msg.worker, msg.meta);
        break;
      default:
        this.log.error(`Action '${msg.action}' is invalid, worker: '${msg.worker}'.`);
        this._statusEventCB(msg.action, msg.file, msg.worker);
    }
  }

  stop(){
    this.log.info('Shutting down dispatcher');
    this.msg.removeAllListeners();
    this.watcher.watcher.removeAllListeners();
    this.msg.close();
    this.watcher.stop();
  }
}

module.exports = Dispatcher;