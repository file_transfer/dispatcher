module.exports = {
    "env": {
        "es6": true,
        "node": true
    },
    "plugins": [
    ],
    "extends": [
      "eslint:recommended",
    ],
    "parserOptions": {
      "ecmaVersion": 8,
      "sourceType": "module",
      "ecmaFeatures": {
        "experimentalObjectRestSpread": true,
      }
    },
    "rules": {
        "no-console": 0,
        "indent": [
            "error",
            2,
            {
              "SwitchCase": 1,
              "VariableDeclarator": {"var": 2, "let": 2, "const": 3}
            }
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
