#!/bin/sh
rm -rf ./tmp/*
mkdir -p ./tmp/integration

echo "Starting dispatcher"
UPLOAD_DIR="./tmp/integration" node index.js &
DISP_PID=$!
sleep 2

ava test/integration/

kill $DISP_PID