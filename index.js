'use_strict';
const Dispatcher = require('./lib/dispatcher'),
      opts = require('./lib/opts');

function handleSignals(sig) {
  dispatcher.log.info(`Caught signal ${sig}, shutting down`);
  dispatcher.stop();
  process.exit(0);
}

process.on('SIGINT', handleSignals);
process.on('SIGTERM', handleSignals);

var dispatcher = new Dispatcher(opts);
async function main() {
  try {
    await dispatcher.start();  
  } catch (error) {
    console.error(`Unable to start dispatcher ${error}`);
    process.exit(1);
  }
}

main();

