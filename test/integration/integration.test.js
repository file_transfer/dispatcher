const test = require('ava'),
      nats = require('nats'),
      {execFile} = require('child_process'),
      fs = require('fs-extra'),
      path = require('path');

var ncWorker,
    msgHost = process.env.MSG_HOST || 'localhost',
    file = 'integration1',
    fileAbs = path.resolve('./tmp/integration/', file);


test.before(async () => {
  ncWorker = nats.connect({servers: [`nats://worker:foo@${msgHost}:4222`]})
    .on('error', (err)=>{ console.error(err); });
});

test.serial.cb('Test adding new file', (t) => {
  t.plan(1);
  let topic = 'workers.queue';

  let sub = ncWorker.subscribe(topic, (msg)=>{
    t.is(JSON.parse(msg).file, file, 'File did not match.');
    ncWorker.unsubscribe(sub);
    t.end();
  });
  setTimeout(()=>{
    execFile('./test/lib/childWrite.js', ['now', fileAbs], (err)=>{
      if (err) {
        t.fail(err);
        t.end();
      }
    });
  }, 10);
});
