#!/usr/bin/env node
const fs = require('fs-extra');

var action = process.argv[2],
    file = process.argv[3];

switch (action) {
  case 'now':
    fs.writeFile(file, 'hi')
      .catch(
        (err) => {
          console.error(err);
          process.exit(1);
        }
      );
    break;
  default:
    console.error(`Invalid file action ${action}`);
    process.exit(1);
}
