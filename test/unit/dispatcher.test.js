const test = require('ava'),
      fs = require('fs-extra'),
      nats = require('nats'),
      {execFile} = require('child_process'),
      path = require('path'),
      Dispatcher = require('../../lib/dispatcher');

var dispatcher,
    ncWorker,
    msgHost = process.env.MSG_HOST || 'localhost',
    watchedDir = path.resolve('./tmp/disp'),
    file = 'disp1',
    fileAbs = path.resolve(watchedDir, file),
    logfile = './tmp/dispatcher.log';

test.before(async () => {
  ncWorker = nats.connect({servers: [`nats://worker:foo@${msgHost}:4222`]})
    .on('error', (err)=>{ console.error(err); });
  try {
    await fs.mkdir(watchedDir, {recursive: true});
  } catch (err) {
    if (err.code !== 'EEXIST') throw err;
  }
  let opts = {
    uploadDir: watchedDir,
    logfile: logfile,
    logLevel: 'debug',
    msgConfig: {
      user: 'dispatcher',
      pass: 'bar',
      host: msgHost,
      port: 4222
    }
  };
  dispatcher = new Dispatcher(opts);
  await dispatcher.start();
});

test.after(()=>{
  dispatcher.stop();
});

test('Test message bus error', async (t)=>{
  t.plan(1);
  let opts = {
    uploadDir: watchedDir,
    msgConfig: {
      host: 'not_a_real_server',
      port: 4222
    }
  };
  let dispTest = new Dispatcher(opts);
  await t.throwsAsync(
    async()=>{ return await dispTest.start.call(dispTest); },
    {code: 'CONN_ERR'},
    'Wrong error type returned'
  );
});

test('Test watcher error', async (t)=>{
  t.plan(1);
  let opts = {
    uploadDir: './package.json',
    msgConfig: {
      host: 'not_a_real_server',
    }
  };
  let dispTest = new Dispatcher(opts);
  await t.throwsAsync(
    async()=>{ return await dispTest.start.call(dispTest); },
    {instanceOf: TypeError},
    'Wrong error type returned'
  );
});


test.serial.cb('Test adding new file', (t) => {
  t.plan(1);
  let topic = 'workers.queue';

  let sub = ncWorker.subscribe(topic, (msg)=>{
    t.is(JSON.parse(msg).file, file, 'File did not match.');
    ncWorker.unsubscribe(sub);
    t.end();
  });
  // Must do this because of event is event change in same proc
  execFile('./test/lib/childWrite.js', ['now', fileAbs], (err)=>{
    if (err) {
      t.fail(err);
      t.end();
    }
  });
});

test.serial.cb('Test status update action uploadStart', (t)=>{
  t.plan(3);
  let msg = {
    action: 'uploadStart',
    file: '/test/uploadStart.txt',
    worker: 'meMyselfAndI',
  };

  dispatcher.statusEventCB = (action, file, worker) =>{
    t.is(action, msg.action, 'Action is incorrect');
    t.is(file, msg.file, 'File is incorrect');
    t.is(worker, msg.worker, 'Worker is incorrect');
    t.end();
  };

  ncWorker.publish('workers.status', JSON.stringify(msg));
});

test.serial.cb('Test status update action uploadProgress', (t)=>{
  t.plan(4);
  let msg = {
    action: 'uploadProgress',
    file: file,
    worker: 'meMyselfAndI',
    meta: '55',
  };

  dispatcher.statusEventCB = (action, file, worker, meta) =>{
    t.is(action, msg.action, 'Action is incorrect');
    t.is(file, msg.file, 'File is incorrect');
    t.is(worker, msg.worker, 'Worker is incorrect');
    t.is(meta, msg.meta, 'Metadata is incorrect');
    t.end();
  };

  ncWorker.publish('workers.status', JSON.stringify(msg));
});

test.serial.cb('Test status update action uploadDone', (t)=>{
  t.plan(3);
  let msg = {
    action: 'uploadDone',
    file: file,
    worker: 'meMyselfAndI',
  };

  dispatcher.statusEventCB = (action, file, worker) =>{
    t.is(action, msg.action, 'Action is incorrect');
    t.is(file, msg.file, 'File is incorrect');
    t.is(worker, msg.worker, 'Worker is incorrect');
    t.end();
  };

  ncWorker.publish('workers.status', JSON.stringify(msg));
});

test.serial.cb('Test status update action uploadError', (t)=>{
  t.plan(4);
  let msg = {
        action: 'uploadError',
        file: 'uploadError.txt',
        worker: 'meMyselfAndI',
        meta: 'I dont feel like uploading',
      },
      testFileAbs = path.resolve(watchedDir, msg.file);

  dispatcher.statusEventCB = (action, file, worker, meta) =>{
    t.is(action, msg.action, 'Action is incorrect');
    t.is(file, msg.file, 'File is incorrect');
    t.is(worker, msg.worker, 'Worker is incorrect');
    t.is(meta, msg.meta, 'Metadata is incorrect');
    t.end();
  };

  execFile('./test/lib/childWrite.js', ['now', testFileAbs], (err)=>{
    if (err) {
      t.fail(err);
      t.end();
    }
    ncWorker.publish('workers.status', JSON.stringify(msg));
  });
});

test.serial.cb('Test status update unsupported action', (t)=>{
  t.plan(3);
  let msg = {
    action: 'KnockKnock',
    file: '/test/KnockKnock.txt',
    worker: 'meMyselfAndI',
  };

  dispatcher.statusEventCB = (action, file, worker) =>{
    t.is(action, msg.action, 'Action is incorrect');
    t.is(file, msg.file, 'File is incorrect');
    t.is(worker, msg.worker, 'Worker is incorrect');
    t.end();
  };

  ncWorker.publish('workers.status', JSON.stringify(msg));
});

// Will just log a message, but increase coverage to 100%
test.serial('Test status update missing attribute', (t)=>{
  let msg = { file: '/test/notThere.txt'};
  ncWorker.publish('workers.status', JSON.stringify(msg));
  t.pass();
});