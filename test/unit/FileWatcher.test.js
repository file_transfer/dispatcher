const test = require('ava'),
      fs = require('fs-extra'),
      winston = require('winston'),
      kc = require('../../lib/knownConsts'),
      execFile = require('child_process').execFile,
      FileWatcher = require('../../lib/FileWatcher'),
      path = require('path');

var watcher,
    logger,
    watchPath = path.resolve('./tmp/fw/'),
    file = 'fw1',
    fileAbs = path.resolve(watchPath, file);

test.before(async () => {
  logger = winston.createLogger({
    level: 'fatal',
    levels: kc.logLevels,
    transports: [new winston.transports.Console()],
  });
  try {
    await fs.mkdir(watchPath, {recursive: true});
  } catch (err) {
    if (err.code !== 'EEXIST') throw err;
  }
  watcher = new FileWatcher(watchPath, logger);
  await watcher.start();
});

test.after(()=>{
  watcher.stop();
});

test.serial('Test for non-existent directory', async (t) => {
  t.plan(1);
  let testWatcher = new FileWatcher('./not/there', logger);
  await testWatcher.start()
    .catch ((err) =>{
      t.is(err.code, 'ENOENT', 'Wrong error code returned');
    });
});

// Monitor directories only
test.serial('Test for monitoring file', async (t) => {
  t.plan(1);
  let testWatcher = new FileWatcher('./package.json', logger);
  await t.throwsAsync(
    async ()=>{ return await testWatcher.start.call(testWatcher); },
    {instanceOf: TypeError},
    'Wrong error type returned'
  );
});


test.serial.cb('Test adding new file', (t) => {
  t.plan(1);
  watcher.on('add', (name) => {
    t.is(name, fileAbs, 'File name is incorrect');
    t.end();
  });
  // Must do this because event is "change" when in same proc
  execFile('./test/lib/childWrite.js', ['now', fileAbs], (err)=>{
    if (err) {
      t.fail(err);
      t.end();
    }
  });
});

test.serial.cb('Test changing file', (t) => {
  t.plan(1);
  watcher.on('change', (name) => {
    t.is(name, fileAbs, 'File name is incorrect');
    t.end();
  });
  fs.writeFile(fileAbs, 'hi', {flags: 'a'});
});
