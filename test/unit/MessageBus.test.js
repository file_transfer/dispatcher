const test = require('ava'),
      nats = require('nats'),
      MessageBus = require('../../lib/MessageBus');

var ncWorker,
    msgHost = process.env.MSG_HOST || 'localhost';

test.before(()=>{
  ncWorker = nats.connect({servers: [`nats://worker:foo@${msgHost}:4222`]})
    .on('error', (err)=>{ console.error(err); });
});

test('Test connect/close method', async(t)=>{
  t.plan(1);
  let mb = new MessageBus({
    user: 'dispatcher',
    pass: 'bar',
    host: msgHost,
    port: '4222'
  });
  await mb.connect();
  mb.close();
  t.pass();
});

test('Test connect method error handling', async (t)=>{
  t.plan(1);
  let mb = new MessageBus({
    host: 'no-server',
    port: '4222'
  });
  try {
    await mb.connect();
  } catch (err) {
    t.is(err.code, 'CONN_ERR', 'Unexpected error code');
  }
});

test('Test dispatchfileUpload method', async(t)=>{
  t.plan(1);
  let mb = new MessageBus({
        tls: false,
        user: 'dispatcher',
        pass: 'bar',
        host: msgHost,
        port: '4222'
      }),
      topic = 'workers.queue',
      file = 'somefile1' ;

  return new Promise(async (resolve, reject) => {
    let sub = ncWorker.subscribe(topic, (msg)=>{
      t.is(JSON.parse(msg).file, file, 'File did not match.');
      ncWorker.unsubscribe(sub);
      mb.close();
      resolve();
    });
    mb.on('error', (err)=>{
      reject(err);
    });
    await mb.connect();
    mb.dispatchfileUpload(file);
  });
});

test('Test status sub', async(t)=>{
  t.plan(1);
  let mb = new MessageBus({
        tls: false,
        user: 'dispatcher',
        pass: 'bar',
        host: msgHost,
        port: '4222'
      }),
      message = JSON.stringify({msg: 'test'});
  let register = new Promise((resolve, reject) => {
    mb
      .on('error', (err)=>{
        t.fail();
        reject(err);
      })
      .on('status', (msg)=>{
        t.is(msg, message, 'Message did not match');
        mb.close();
        resolve();
      });
  });

  await mb.connect();
  ncWorker.publish('workers.status', message);
  return register;
});